document.addEventListener("DOMContentLoaded", init);

function init(){
	var canvas = document.getElementById('pengineCanvas');
	var pe = new PEngine(canvas);
	pe.addParticles(20);
	pe.quad = new QuadTree({
		x1: 0,
		y1: 0,
		x2: canvas.width,
		y2: canvas.height
	});
	pe.animate();
}

var PEngine = function(canvas){
	this.canvas = canvas;
	this.context = this.canvas.getContext('2d');
	this.radomRadius = true;
	this.randomColor = false;
	this.fps = 30;
	this.particles = [];
	this.particleConfig = {
		velocity: 0.2,
		minRadius: 10, // 5,
		maxRadius: 10, //15,
		particleBorder: true,
		particleFill: true,
		borderWidth: 2,
		borderColor: '#000000',
		hslaColor: {
			hue: '0', 
			saturation: '0%', 
			lightness: '50%', 
			alpha: '0.5'
		}
	};
}

PEngine.prototype.getColor = function(){
	var color = this.particleConfig.hslaColor,
	hue = (this.randomColor ? Math.random()*360 : color.hue);
	return 'hsla(' + hue + ',' + color.saturation + ',' + color.lightness + ',' + color.alpha + ')';
}

PEngine.prototype.addParticles = function(numParticles){
	while(numParticles--){
		var safeDist = this.particleConfig.minRadius+this.particleConfig.maxRadius;
		particle = new Particle({
			canvas: this.canvas,
			context: this.context,
			particleBorder: this.particleConfig.particleBorder,
			particleFill: this.particleConfig.particleFill,
			velX: this.randDir()*Math.random()*60,
			velY: this.randDir()*Math.random()*60,
			vel: this.particleConfig.velocity,
			posX: Math.random()*(this.canvas.width - safeDist*2) + safeDist,
			posY: Math.random()*(this.canvas.height - safeDist*2) + safeDist,
			color: this.getColor(), 
			borderWidth: this.particleConfig.borderWidth, 
			borderColor: this.particleConfig.borderColor,
			radius: this.radomRadius ? (Math.random()*this.particleConfig.maxRadius+this.particleConfig.minRadius) : this.particleConfig.minRadius
		});
		particle.drawParticle();
		this.particles.push(particle);
	}
}

PEngine.prototype.randDir = function(){
	return Math.random() * 2 - 1;
}

PEngine.prototype.updateParticles = function(){
	this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	for(var index in this.particles){
		this.particles[index].updateParticle();
	}
}

PEngine.prototype.animate = function(){
	var self = this;
	setInterval(function(){ 
		self.updateParticles();
		self.quad.clear();
		self.quad.insert(self.particles);
		self.detectCollisions(self.quad.getTree());
	}, 1000/self.fps);
}

PEngine.prototype.detectCollisions = function(tree){
	if(!Object.keys(tree).length){
		return;
	}
	if(tree.nodes.length){
		for(var i in tree.nodes){
			this.detectCollisions(tree.nodes[i]);
		}
	}else if(Object.keys(tree.objects).length){
		for(var i = 0; i < tree.objects.length; ++i){
			for(var j = i+1; j < tree.objects.length; ++j){
				var p1 = {
					x: tree.objects[i].posX - tree.objects[i].radius,
					y: tree.objects[i].posY - tree.objects[i].radius,
					w: (tree.objects[i].posX + tree.objects[i].radius) - (tree.objects[i].posX - tree.objects[i].radius),
					h: (tree.objects[i].posY + tree.objects[i].radius) - (tree.objects[i].posY - tree.objects[i].radius)
				}
				var p2 = {
					x: tree.objects[j].posX - tree.objects[j].radius,
					y: tree.objects[j].posY - tree.objects[j].radius,
					w: (tree.objects[j].posX + tree.objects[j].radius) - (tree.objects[j].posX - tree.objects[j].radius),
					h: (tree.objects[j].posY + tree.objects[j].radius) - (tree.objects[j].posY - tree.objects[j].radius)
				}
				// hit test
				if(((p1.x + p1.w >= p2.x) && (p1.x <= p2.x + p2.w)) && ((p1.y + p1.h >= p2.y) && (p1.y <= p2.y + p2.h))){
					tree.objects[i].collide();
					tree.objects[j].collide();
				}
			}
		}
	}
}

var Particle = function(config){
	for (var property in config){ 
		this[property] = config[property]; 
	}
}

Particle.prototype.drawParticle = function(){
	this.context.beginPath();
	this.context.arc(this.posX, this.posY, this.radius, 0, Math.PI *2, false);
	this.context.closePath();
	if(this.particleBorder){
		this.context.lineWidth = this.borderWidth;
		this.context.strokeStyle = this.borderColor;
		this.context.stroke();
	}
	if(this.particleFill){
		this.context.fillStyle = this.color;
		this.context.fill();
	}
}

Particle.prototype.updateParticle = function(){
	this.detectBorderCollision();
	this.posX += this.velX * this.vel;
	this.posY += this.velY * this.vel;
	this.drawParticle(this.posX, this.posY, this.radius, this.color, this.borderWidth, this.borderColor);
}

Particle.prototype.detectBorderCollision = function(){
	if(this.posX-this.radius < 0 || this.posX+this.radius > this.canvas.width){
		this.velX = -this.velX;
	}
	if(this.posY-this.radius < 0 || this.posY+this.radius > this.canvas.height){
		this.velY = -this.velY;
	}
}

Particle.prototype.collide = function(){
	this.velX = -this.velX;
	this.velY = -this.velY;
}

// using getPosition to make this more generic
Particle.prototype.getPosition = function(){
	return {
		x: this.posX,
		y: this.posY
	};
}