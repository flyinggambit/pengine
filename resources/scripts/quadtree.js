var QuadTree = function(bounds, level){
	this.MAX_OBJECTS = 10;
	this.MAX_LEVELS = 5;
	this.level = level || 0;
	this.bounds = bounds;
	this.objects = [];
	this.nodes = [];
}

QuadTree.prototype.insert = function(obj){
	this.objects = this.objects.concat(obj);
	if(this.objects.length > this.MAX_OBJECTS && this.level <= this.MAX_LEVELS){
		if(this.nodes.length === 0){
			this.split();
		}
		// Need to insert items one by one as we have to find the quadrant index of each item
		var index = this.objects.length;
		while(index--){
			var quandrantIndex = this.getQuadrantIndex(this.objects[index].getPosition());
			if(quandrantIndex !== -1){
				this.nodes[quandrantIndex].insert(this.objects.pop(this.objects[index]));
			}
		}
	}
}

QuadTree.prototype.split = function(){
	var quadrantWidth = this.bounds.x2 - this.bounds.x1;
	var quadrantHeight = this.bounds.y2 - this.bounds.y1;
	
	// top left quandrant
	this.nodes[0] = new QuadTree({
		x1: this.bounds.x1,
		y1: this.bounds.y1,
		x2: this.bounds.x1 + quadrantWidth/2,
		y2: this.bounds.y1 + quadrantHeight/2
	}, this.level+1);

	// top right quandrant
	this.nodes[1] = new QuadTree({
		x1: this.bounds.x1 + quadrantWidth/2,
		y1: this.bounds.y1,
		x2: this.bounds.x2,
		y2: this.bounds.y1 + quadrantHeight/2
	}, this.level+1);

	// bottom left quandrant
	this.nodes[2] = new QuadTree({
		x1: this.bounds.x1,
		y1: this.bounds.y1 + quadrantHeight/2,
		x2: this.bounds.x1 + quadrantWidth/2,
		y2: this.bounds.y2
	}, this.level+1);

	// bottom right quandrant
	this.nodes[3] = new QuadTree({
		x1: this.bounds.x1 + quadrantWidth/2,
		y1: this.bounds.y1 + quadrantHeight/2,
		x2: this.bounds.x2,
		y2: this.bounds.y2
	}, this.level+1);
}

// This function will be called only after splitting the quandrant
QuadTree.prototype.getQuadrantIndex = function(objPos){
	for(var i = 0; i < 4; ++i){
		if(objPos.x > this.nodes[i].bounds.x1 && objPos.x < this.nodes[i].bounds.x2 && objPos.y > this.nodes[i].bounds.y1 && objPos.y < this.nodes[i].bounds.y2){
			return i;
		}
	}
	return -1;
}

QuadTree.prototype.clear = function(){
	this.objects = [];
	this.nodes = [];
}

QuadTree.prototype.getTree =  function(){
	var nodes = [];
	if(this.nodes.length > 0){
		nodes = [this.nodes[0].getTree(), this.nodes[1].getTree(), this.nodes[2].getTree(), this.nodes[3].getTree()]
	}else{
		nodes = this.nodes;
	}
	return {
		objects: this.objects,
		nodes: nodes
	}
}