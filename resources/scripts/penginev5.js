document.addEventListener("DOMContentLoaded", init);

function init(){
	var canvas = document.getElementById('pengineCanvas');
	var pe = new PEngine(canvas);
	pe.addParticles(20);
	pe.animate();
}

var PEngine = function(canvas){
	this.canvas = canvas;
	this.context = this.canvas.getContext('2d');
	this.radomRadius = true;
	this.randomColor = false;
	this.fps = 30;
	this.particles = [];
	this.particleConfig = {
		minRadius: 5,
		maxRadius: 15,
		particleBorder: true,
		particleFill: true,
		borderWidth: 2,
		borderColor: '#000000',
		hslaColor: {
			hue: '0', 
			saturation: '0%', 
			lightness: '50%', 
			alpha: '0.5'
		}
	};
}

PEngine.prototype.getColor = function(){
	var color = this.particleConfig.hslaColor,
	hue = (this.randomColor ? Math.random()*360 : color.hue);
	return 'hsla(' + hue + ',' + color.saturation + ',' + color.lightness + ',' + color.alpha + ')';
}

PEngine.prototype.addParticles = function(numParticles){
	while(--numParticles){
		var posX = Math.random()*this.canvas.width,
			posY = Math.random()*this.canvas.height,
			radius = this.radomRadius ? (Math.random()*this.particleConfig.maxRadius+this.particleConfig.minRadius) : this.particleConfig.minRadius,
			particle = new Particle(this.context, this.particleConfig.particleBorder, this.particleConfig.particleFill, this.randDir()*30, this.randDir()*60, 0.05);
		particle.drawParticle(posX, posY, radius, this.getColor(), this.particleConfig.borderWidth, this.particleConfig.borderColor);
		this.particles.push(particle);
	}
}

PEngine.prototype.randDir = function(){
	return Math.random() * 2 - 1;
}

PEngine.prototype.updateParticles = function(){
	this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	for(var index in this.particles){
		this.particles[index].updateParticle();
	}
}

PEngine.prototype.animate = function(){
	var self = this;
	setInterval(function(){ 
		self.updateParticles();
	}, 1000/self.fps);
}

var Particle = function(context, particleBorder, particleFill, velX, velY, vel){
	this.context = context;
	this.particleBorder = particleBorder;
	this.particleFill = particleFill;
	this.velX = velX, 
	this.velY = velY, 
	this.vel = vel;
}

Particle.prototype.drawParticle = function(posX, posY, radius, color, borderWidth, borderColor){
	this.posX = posX;
	this.posY = posY;
	this.radius = radius;
	this.color = color;
	this.borderWidth = borderWidth;
	this.borderColor = borderColor;

	this.context.beginPath();
	this.context.arc(posX, posY, radius, 0, Math.PI *2, false);
	this.context.closePath();
	if(this.particleBorder){
		this.context.lineWidth = borderWidth;
		this.context.strokeStyle = borderColor;
		this.context.stroke();
	}
	if(this.particleFill){
		this.context.fillStyle = color;
		this.context.fill();
	}
}

Particle.prototype.updateParticle = function(){
	this.posX += this.velX * this.vel;
	this.posY += this.velY * this.vel;
	this.drawParticle(this.posX, this.posY, this.radius, this.color, this.borderWidth, this.borderColor);
}